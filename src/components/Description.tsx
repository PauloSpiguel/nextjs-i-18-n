import React, { FunctionComponent } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

import { Trans } from "../i18n";

const Wrapper = styled.div<{ noLineBreakMaxWidth: number }>`
  padding-top: 12px;
  padding-bottom: 32px;

  a {
    white-space: nowrap;
  }

  @media (max-width: ${(p) => p.noLineBreakMaxWidth}px) {
    text-align: left;

    br {
      content: "";
    }
    br:after {
      content: " ";
    }
  }
`;

const Description: FunctionComponent<{ children?: never }> = () => {
  const { i18n } = useTranslation();

  return (
    <Wrapper noLineBreakMaxWidth={i18n.language === "ru" ? 500 : 310}>
      <Trans i18nKey="index:description.l1" />
      <br />
      <Trans i18nKey="index:description.l2">
        <a href="http://www.gicentre.net/">giCentre</a>
      </Trans>
      <br />
      <Trans i18nKey="index:description.l3" />
    </Wrapper>
  );
};

export default Description;
