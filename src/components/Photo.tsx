import Image from "next/image";
import React, { FunctionComponent } from "react";
import styled from "styled-components";

import photo from "../../public/images/alexander_kachkaev.jpg";
import { useTranslation } from "../i18n";

const Wrapper = styled.div`
  float: right;
  margin-left: 10px;
`;

const Img = styled(Image)`
  display: block;
  border-radius: 5px;
  overflow: hidden;
  background: #ccc;
  color: #fff;
`;

const Photo: FunctionComponent<{ children?: never }> = () => {
  const { t } = useTranslation();

  return (
    <Wrapper>
      <Img
        priority={true}
        width="100"
        height="100"
        alt={t("index:photoAlt")}
        src={photo}
      />
    </Wrapper>
  );
};

export default Photo;
