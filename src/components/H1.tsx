import styled from "styled-components";

const H1 = styled.h1<{ error?: boolean }>`
  font-size: 26px;
  line-height: 30px;
`;

export default H1;
