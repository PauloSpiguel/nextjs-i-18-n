import "../lib/pageEvents";

import { ApolloProvider } from "@apollo/react-hooks";
import { NormalizedCacheObject } from "apollo-cache-inmemory";
import ApolloClient from "apollo-client";
import App from "next/app";
import Head from "next/head";
import * as React from "react";
import styled from "styled-components";

import GlobalStyle from "../components/GlobalStyle/GlobalStyle";
import LinkToSources from "../components/LinkToSource";
import LocaleToggler from "../components/LocaleToggler";
import { appWithTranslation } from "../i18n";
import { withApolloClient } from "../lib/apollo";

const Wrapper = styled.div`
  display: table;
  width: 100%;
  height: 100%;
  position: relative;
`;

class MyApp extends App<{ apolloClient: ApolloClient<NormalizedCacheObject> }> {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    const { hostByLocale } =
      ctx.req || (window as any).__NEXT_DATA__.props.pageProps;

    return {
      pageProps: { ...pageProps, hostByLocale },
    };
  }

  render() {
    const { Component, apolloClient, pageProps } = this.props;

    return (
      <ApolloProvider client={apolloClient}>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>
        <GlobalStyle />
        <Wrapper>
          <LocaleToggler {...pageProps} />
          <Component {...pageProps} />
          <LinkToSources />
        </Wrapper>
      </ApolloProvider>
    );
  }
}

const WrappedApp = withApolloClient(appWithTranslation(MyApp));
export default WrappedApp;
