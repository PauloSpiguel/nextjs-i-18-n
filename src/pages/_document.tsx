import Document, { Head, Html, Main, NextScript } from "next/document";
import * as React from "react";
import { ServerStyleSheet } from "styled-components";

const InlineJs: React.FunctionComponent<{ code: string; children?: never }> = ({
  code,
}) => {
  return (
    <script
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: code.replace(/\/\*.*\*\//g, " ").replace(/\s+/g, " "),
      }}
    />
  );
};

export default class MyDocument extends Document<{
  styleTags;
  gaTrackingId;
  locale;
}> {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          // eslint-disable-next-line react/display-name
          enhanceApp: (App) => (props) =>
            sheet.collectStyles(<App {...props} />),
        });

      const initialProps = await Document.getInitialProps(ctx);

      return {
        ...initialProps,
        locale: ctx.req.locale,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
        gaTrackingId: ctx.req.gaTrackingId,
        hostByLocale: ctx.req.hostByLocale,
        graphqlUri: ctx.req.graphqlUri,
      };
    } finally {
      sheet.seal();
    }
  }

  static getInitialProps2({ renderPage, req }: { renderPage; req? }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(
      // eslint-disable-next-line react/display-name
      (App) => (props) => sheet.collectStyles(<App {...props} />),
    );
    const styleTags = sheet.getStyleElement();

    return {
      ...page,
      locale: req.locale,
      styleTags,
      gaTrackingId: req.gaTrackingId,
      hostByLocale: req.hostByLocale,
      graphqlUri: req.graphqlUri,
    };
  }

  render() {
    // https://realfavicongenerator.net
    return (
      <Html lang={this.props.locale}>
        <Head>
          <meta charSet="utf-8" />
          <link rel="preconnect" href="https://www.google-analytics.com" />
          <meta
            name="apple-mobile-web-app-status-bar-style"
            content="black-translucent"
          />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/favicon/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon/favicon-16x16.png"
          />
          <link rel="manifest" href="/manifest.json" />
          <link
            rel="mask-icon"
            href="/favicon/safari-pinned-tab.svg"
            color="#ffffff"
          />
          <meta name="msapplication-config" content="/browserconfig.xml" />
          <meta name="theme-color" content="#ffffff" />
          <link rel="shortcut icon" href="/favicon.ico" />
          {this.props.styleTags}
          {this.props.gaTrackingId ? (
            <>
              <script
                key="ga1"
                async={true}
                src={`https://www.googletagmanager.com/gtag/js?id=${this.props.gaTrackingId}`}
              />
              <InlineJs
                key="ga2"
                code={`
window.dataLayer = window.dataLayer || [];
window.gaTrackingId = '${this.props.gaTrackingId}';
function gtag(){
  dataLayer.push(arguments);
}
gtag('js', new Date());
gtag('config', window.gaTrackingId);`}
              />
            </>
          ) : (
            <InlineJs
              code={`
function gtag(){
  console.log('dummy gtag call', arguments)
}`}
            />
          )}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
