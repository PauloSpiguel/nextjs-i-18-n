/* eslint-disable @typescript-eslint/no-var-requires */

import ICU from "i18next-icu";
import en from "i18next-icu/locale-data/en";
import ru from "i18next-icu/locale-data/ru";
import _ from "lodash";
import { NextComponentType, NextPageContext } from "next";
import NextI18next from "next-i18next";
import getConfig from "next/config";
import { useTranslation as originalUseTranslation } from "react-i18next";

const use: any[] = [];
const icu = new ICU({});
icu.addLocaleData(ru);
icu.addLocaleData(en);
use.push(icu);

let detectionOrder: string[] = [];
if (
  typeof window === "undefined" &&
  process.env.NEXT_PHASE !== "phase-production-build"
) {
  const { env } = require("./config");
  const i18nextMiddleware = require("i18next-express-middleware");
  const languageDetector = new i18nextMiddleware.LanguageDetector(null, {
    order: ["enforcedLocale", "languageByDomain"],
  });

  languageDetector.addDetector({
    name: "enforcedLocale",
    lookup: () => env.ENFORCED_LOCALE,
    cacheUserLanguage: _.noop,
  });

  languageDetector.addDetector({
    name: "languageByDomain",
    lookup: (opts) => {
      const hostWithoutPort = (
        opts.headers["x-forwarded-host"] ||
        opts.headers.host ||
        ""
      ).replace(/:\d+$/, "");

      return hostWithoutPort === getConfig().publicRuntimeConfig.hostRu
        ? "ru"
        : "en";
    },
    cacheUserLanguage: _.noop,
  });
  use.push(languageDetector);
  detectionOrder = ["enforcedLocale", "languageByDomain"];
}

let lang = "en";

// TODO: Upgrade next-i18next to avoid using this hack
if (typeof window !== "undefined") {
  try {
    const nextData = JSON.parse(
      window.document.getElementById("__NEXT_DATA__")?.innerHTML ?? "{}",
    );

    lang = nextData.props.initialLanguage ?? lang;
  } catch (e) {
    // noop
  }
}

export const nextI18next = new NextI18next({
  defaultLanguage: lang,
  fallbackLng: lang,
  // eslint-disable-next-line @typescript-eslint/naming-convention
  defaultNS: "common",
  otherLanguages: ["ru", "en"],
  localePath: typeof window === "undefined" ? "public/locales" : "locales",
  keySeparator: "###",
  use,
  browserLanguageDetection: false,
  detection: {
    lookupCookie: null,
    order: detectionOrder,
  },
});

export const includeDefaultNamespaces = (namespaces: string[]) =>
  ["common", "_error"].concat(namespaces);

export const appWithTranslation = nextI18next.appWithTranslation;
export const Trans = nextI18next.Trans;
export const useTranslation = originalUseTranslation;
export type I18nPage<P = Record<string, never>> = NextComponentType<
  NextPageContext,
  { namespacesRequired: string[] },
  P & { namespacesRequired: string[] }
>;
